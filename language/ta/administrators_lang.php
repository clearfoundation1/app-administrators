<?php

$lang['administrators_app_description'] = 'நிர்வாகிகள் பயன்பாட்டை, நீங்கள் கணினியில் பயனர் குழுக்களுக்கு குறிப்பிட்ட பயன்பாடுகளுக்கான அணுகலை வழங்க முடியும்.';
$lang['administrators_app_name'] = 'நிர்வாகிகள்';
