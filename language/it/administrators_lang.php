<?php

$lang['administrators_app_description'] = 'Con l&#39;applicazione amministratori, è possibile concedere l&#39;accesso alle applicazioni specifiche per gruppi di utenti del sistema.';
$lang['administrators_app_name'] = 'Gli amministratori';
