<?php

$lang['administrators_app_description'] = 'Avec l&#39;application Administrateurs, vous pouvez autoriser l&#39;accès à des applications spécifiques à des groupes d&#39;utilisateurs sur le système.';
$lang['administrators_app_name'] = 'Administrateurs';
